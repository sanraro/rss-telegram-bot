require 'telegram/bot'
require_relative 'crawler/rss'

class RssBot
  def self.run
    Telegram::Bot::Client.run(Application.config[:token]) do |bot|
      begin
        bot.listen do |message|
          begin
            if !message.text.nil?
              username = message.from.username ? "@#{message.from.username}" : message.from.first_name
              channel = Channel.find_or_create_by(uid: message.chat.id) do |u|
                u.username = username
                u.language = message.from.language_code || I18n.default_locale 
              end
              Log.debug "MSG recived from #{channel.uid} --> #{message.text}"
              case message.text
              when /\/start/i
                username = message.from.username ? "@#{message.from.username}" : message.from.first_name
                if channel.subscriptions.any?
                  bot.api.send_message chat_id: channel.uid, text: "Hello again, #{username}"
                  bot.api.send_message chat_id: channel.uid, text: "I think we have already been introduced"
                else 
                  bot.api.send_message chat_id: channel.uid, text: "Hello #{username}, nice to meet you"
                  if url = Application.config[:default_rss]
                    rss = RssFeed.find_or_create_by url: url
		    Subscription.find_or_create_by(channel_id: channel.id, rss_feed_id: rss.id) {|channel| channel.name = name}
                    bot.api.send_message chat_id: channel.uid, text: "I have subscribed you to #{url} RSS feed"
                  end
                end
              when /\/add[ ]+([^ ]+)[ ]+([^ ]+)/i
                match = message.text.match(/^\/add[ ]+([^ ]+)[ ]+([^ ]+)/i)
                name = match[1]
                url = match[2]
                rss = RssFeed.find_or_create_by url: url
                if rss.channels.find_by_id channel.id
                  bot.api.send_message chat_id: channel.uid, text: "#{url} was already added"
                else
                  Subscription.create channel_id: channel.id, rss_feed_id: rss.id, name: name
                  bot.api.send_message chat_id: channel.uid, text: "#{name} added successfuly (url: #{url})"
                end
                self.send self.crawl(channel.rss_feeds)
              when /\/remove[ ]+([^ ]+)/i
                name = message.text.match(/\/remove[ ]+([^ ]+)/i)[1]
                if subscription = Subscription.find_by(name: name, channel_id: channel.id)
                  # Destroy feed if there is only current channel subscribed
                  subscription.rss_feed.destroy if Subscription.where(rss_feed_id: subscription.rss_feed_id).size == 1
                  # Destroy subscription
                  subscription.destroy
                  bot.api.send_message chat_id: channel.uid, text: "#{name} removed successfuly"
                else
                  bot.api.send_message chat_id: channel.uid, text: "#{name} is not defined"
                end
              when /\/all/i
                subscriptions = channel.subscriptions
                message = subscriptions.empty? ? "You don't have any subscription yet" : subscriptions.collect{|sub| "- #{sub.name} : #{sub.rss_feed.url}"}.join("\n")
                bot.api.send_message chat_id: channel.uid, text: message 
              when /\/me/i
                bot.api.send_message chat_id: channel.uid, text: "Your are #{username}, registered as #{channel.username}, aren't you?"
              end
            end
          end
        end
      rescue Telegram::Bot::Exceptions::ResponseError => error
        puts error
        puts error.backtrace
        # retry%1
      end
    end
  end

  def self.cron
    while true
      # TODO: Crawl active crawlers
      Log.info("Start crawl")
      self.send self.crawl
      sleep (Application.config[:delay]||180).to_i
    end
  end 

  def self.crawl feeds=nil
    crawled = {}
    # TODO: Multiple crawlers
    rss = Crawler::RSS.new
    feeds ||= RssFeed.all
    
    feeds.each do |crawl|
      current_crawleds = []
      begin
        rss.get(crawl.url).each do |item|
          c_crawled = {}
          c_crawled[:title] = item.title&.content || ""
          c_crawled[:description] = item.description&.content || ""
          c_crawled[:link] = item.link&.content || "" 
          # TODO: Remove this hardcoded fix
          c_crawled[:link] = c_crawled[:link].sub "flux-rss/", "" 
          c_crawled[:link] = /https?:\/\/[^\/]+/.match(crawl.url)[0] + c_crawled[:link] if c_crawled[:link].start_with? "/"
          # c_crawled[:guid] = item.guid&.content
          c_crawled[:pubDate] = DateTime.parse(item.pubDate&.content).to_time
          current_crawleds << c_crawled
        end
      rescue
        Log.error("Could not crawl #{crawl.url}")
      end
      crawled[crawl.id] = current_crawleds
      crawl.save
    end
    return crawled
  end

  def self.send crawled
    Telegram::Bot::Client.run(Application.config[:token]) do |bot|
      crawled.each do |feed_id, feed_items|
        items = feed_items.first(5).reverse
        # Send a message for each of channels subscribed
        Subscription.where(rss_feed_id: feed_id).each do |sub|
          items.each do |item|
            msg = "#{item[:title]} (#{I18n.l(item[:pubDate].to_date)})\n#{item[:description]}\n#{item[:link]}"
	    bot.api.send_message(chat_id: sub.channel.uid, text: msg) if sub.readed_at.nil? || item[:pubDate] > sub.readed_at
          end
	  sub.update_attribute :readed_at, Time.now
        end
      end
    end
  end

end
