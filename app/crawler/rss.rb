# Adapted from https://gitlab.com/Cooq/rss-bot
require_relative 'base'

class Crawler::RSS < Crawler::Base
  @@accepted_urls = [/^https?:\/\/.+\.xml$/, /^https?:\/\/.+\.rss$/]
  def get url
    return self.xml_request(url).css("item")
  end
end
# a = Crawler::RSS.new 
# a.get("https://mangapark.net/rss/latest.xml")
