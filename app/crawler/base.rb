# Adapted from https://gitlab.com/Cooq/rss-bot/
require 'json'
require 'net/http'
require 'logger'
require 'nokogiri'
require_relative '../crawler'

class Crawler::Base
  def initialize
  end

  @@accepted_urls = []

  def http_request link
    Log.debug("REQUEST")
    Log.debug(link)
    return Net::HTTP.get(URI(link))
  end
  
  # Parse HTTP JSON request
  def json_request link
    Log.debug("JSON")
    return JSON.parse(http_request(link))
  end

  # Parse HTTP XML request
  def xml_request link
    Log.debug("XML")
    return Nokogiri.Slop(http_request(link))
  end

  def accept? url
    @@accepted_urls.each do |u|
      match = u.match(url)
      return true if ! match.nil?
    end
    return false
  end

  # Return Crawled Object
  def get url
    # Overwrited with each crawler
  end
end
