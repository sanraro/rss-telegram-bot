class Channel < ApplicationRecord
  has_many :subscriptions, dependent: :destroy
  has_many :rss_feeds, through: :subscriptions
end
