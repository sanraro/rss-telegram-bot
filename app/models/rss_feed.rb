class RssFeed < ApplicationRecord
  has_many :subscriptions, dependent: :destroy
  has_many :channels, through: :subscriptions

  def to_s
    self.url
  end

  def self._remove(channel_id, name)
    relation = Relation.find_by(name: name)
    relation.destroy if relation 
  end

  def self._add(channel_id, name, url)
    crawler = Crawler.find_by(url: url)
    if not crawler then
      crawler = Crawler.new
      crawler.url = url
      crawler.save
    end
    begin
      relation = Relation.new
      relation.channel_id = channel_id
      relation.crawler = crawler
      relation.name = name
      relation.last_updated = 0
      relation.save
      Log.info relation
      return true
    rescue ActiveRecord::RecordNotUnique => error
      Log.error "Multiple relation"
      return false
    end
  end
end
