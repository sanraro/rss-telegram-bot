class Subscription < ApplicationRecord
  belongs_to :channel
  belongs_to :rss_feed
end
