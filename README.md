# RSS Telegram bot
You can follow the rss you want

## Runnning
```bash
bundle install
rake db:migrate
bin/rss-bot
```

## Build docker image
```bash
docker build -t rss-telegram-bot .
docker run -e "TOKEN=my-telegram-token" -e "DELAY=delay-in-seconds" -d rss-telegram-bot
```
