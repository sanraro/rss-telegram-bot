require 'erb'
require_relative 'boot'
require_relative 'persistence'

# Read bot config (mainly bot token)
module Application
  @@config = HashWithIndifferentAccess.new YAML.load(ERB.new(File.read('config/bot.yml')).result)
  def self.config
    return @@config
  end
  def self.db_migrate
    conn = ActiveRecord::Base.connection
    # Create migrations table if not exists
    if !conn.data_source_exists? 'schema_migrations'
      Log.info "Creating migrations table"
      conn.execute('create table schema_migrations (version varchar(255), primary key(version))')
    end

    Log.info "Appliying database changes"
    Dir['db/migrate/*.rb'].each do |file|
      if obj = file.match(/^db\/migrate\/([0-9]+)_([^.]+)\.rb$/)
        id = obj[1]
        classname = obj[2].split('_').collect!{ |w| w.capitalize }.join
	if conn.execute("SELECT version FROM schema_migrations where version=#{id}").blank?
          require_relative "../#{file}"
          eval("#{classname}.migrate(:up)")
          conn.execute("INSERT INTO schema_migrations VALUES (#{id})")
        end
      end
    end
  end	
end
# Load main app
require_relative "../app/rss_bot.rb"
