# Initialize database
require 'sqlite3'
require 'active_record'
Log.info "Connecting to database"
ActiveRecord::Base.establish_connection( adapter: 'sqlite3', database: 'db/storage/sqlite.db' )

# Load models
Log.info "Loading models"
require "./app/models/application_record.rb"
Dir['app/models/*.rb'].each do |file|
  require "./#{file}"
end
