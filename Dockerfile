FROM ruby:2.6-alpine

# Set the base directory used in any further RUN, COPY, and ENTRYPOINT
# commands.
RUN mkdir -p /bot
WORKDIR /bot

# Copy dependencies into the container
COPY Gemfile Gemfile.lock ./ 

RUN apk add --update ruby-dev build-base sqlite sqlite-dev sqlite-libs
RUN gem install bundler && bundle install --jobs 20 --retry 5 --without development test
RUN apk del build-base ruby-dev sqlite-dev

# Copy the main application into the container
COPY . ./
COPY config/bot.yml.example config/bot.yml

# Expose a volume with sqlite database 
VOLUME ["/bot/db/storage"]

# Start the application
CMD ["bundle","exec","bin/rss-bot"]
