class CreateSubscriptions < ActiveRecord::Migration[6.0]
  def change
    create_table :subscriptions do |t|
      t.string :name
      t.references :channel
      t.references :rss_feed
      t.timestamps
    end
    add_index :subscriptions, [:channel_id, :rss_feed_id], unique: true
  end
end
