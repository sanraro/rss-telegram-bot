class CreateRssFeeds < ActiveRecord::Migration[6.0]
  def change
    create_table :rss_feeds do |t|
      t.string :url, index: {unique: true}
      t.timestamps
    end
  end
end
