class CreateChannels < ActiveRecord::Migration[6.0]
  def change
    create_table :channels do |t|
      t.string :uid, index: {unique: true}
      t.string :username
      t.string :language
      t.timestamps
    end
  end
end
